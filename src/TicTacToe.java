import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
        boolean kazanan = true;
        while(kazanan){
            printBoard(board);

            while (true) {
                System.out.print("Player 1 enter row number:");
                int row = reader.nextInt();
                System.out.print("Player 1 enter column number:");
                int col = reader.nextInt();
                boolean x = true;



                while (x) {



                    if (row < 4 && row > 0 && col < 4 && col > 0 && board[row - 1][col - 1] == ' ') {
                        board[row - 1][col - 1] = 'X';
                        printBoard(board);
                        x = false;
                    } else {
                        System.out.println("Try Again...");
                        System.out.print("Player 1 enter row number:");
                        row = reader.nextInt();
                        System.out.print("Player 1 enter column number:");
                        col = reader.nextInt();

                        while (row > 3 || row < 1 || col > 3 || col < 1 || board[row - 1][col - 1] == 'X' || board[row - 1][col - 1] == 'O') {
                            System.out.println("Try again...");
                            System.out.print("Player 1 enter row number:");
                            row = reader.nextInt();
                            System.out.print("Player 1 enter column number:");
                            col = reader.nextInt();

                        }
                        board[row - 1][col - 1] = 'X';

                        printBoard(board);
                        x = false;
                    }

                }


                kazanan=checkboard(board);

                if(kazanan==false)
                    break;

                System.out.print("Player 2 enter row number:");
                row = reader.nextInt();
                System.out.print("Player 2 enter column number:");
                col = reader.nextInt();
                boolean y = true;
                while (y) {
                    if (row < 4 && row > 0 && col < 4 && col > 0 && board[row - 1][col - 1] == ' ') {
                        board[row - 1][col - 1] = 'O';
                        printBoard(board);
                        y = false;
                    } else {
                        System.out.println("Try again...");
                        System.out.print("Player 2 enter row number:");
                        row = reader.nextInt();
                        System.out.print("Player 2 enter column number:");
                        col = reader.nextInt();
                        while (row > 3 || row < 1 || col > 3 || col < 1 || board[row - 1][col - 1] == 'X' || board[row - 1][col - 1] == 'O') {
                            System.out.println("Try again...");
                            System.out.print("Player 2 enter row number:");
                            row = reader.nextInt();
                            System.out.print("Player 2 enter column number:");
                            col = reader.nextInt();
                        }
                        board[row - 1][col - 1] = 'O';

                        printBoard(board);
                        y = false;
                    }

                }
                kazanan=checkboard(board);
                if(kazanan==false)
                    break;

            }
        }
    }
    public static void printBoard(char[][] board) {
        System.out.println("    1   2   3");
        System.out.println("   -----------");
        for (int row = 0; row < 3; ++row) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < 3; ++col) {
                System.out.print("|");
                System.out.print(" " + board[row][col] + " ");
                if (col == 2)
                    System.out.print("|");

            }
            System.out.println();
            System.out.println("   -----------");

        }



    }

    public static boolean checkboard(char[][] board) {

        if ((board[0][0] == board[0][1] && board[0][0] == board[0][2] && (board[0][0]=='X' )) || (board[1][0] == board[1][1] && board[1][0] == board[1][2])&& (board[1][0]=='X' ) ||
                (board[2][0] == board[2][1] && board[2][0] == board[2][2])&& (board[2][0]=='X' ) || (board[0][0] == board[1][0] && board[0][0] == board[2][0])&& (board[0][0]=='X' ) ||
                (board[0][1] == board[1][1] && board[0][1] == board[2][1])&& (board[0][1]=='X' ) || (board[0][2] == board[1][2] && board[0][2] == board[2][2])&& (board[0][2]=='X' ) ||
                (board[0][0] == board[1][1] && board[0][0] == board[2][2])&& (board[0][0]=='X' ) || (board[0][2] == board[1][1] && board[0][2] == board[2][0])&& (board[0][2]=='X' )){


            System.out.println("First player won");

            return false;
        }

        else if ((board[0][0] == board[0][1] && board[0][0] == board[0][2] && (board[0][0]=='O' )) || (board[1][0] == board[1][1] && board[1][0] == board[1][2])&& (board[1][0]=='O' ) ||
                (board[2][0] == board[2][1] && board[2][0] == board[2][2])&& (board[2][0]=='O' ) || (board[0][0] == board[1][0] && board[0][0] == board[2][0])&& (board[0][0]=='O' ) ||
                (board[0][1] == board[1][1] && board[0][1] == board[2][1])&& (board[0][1]=='O' ) || (board[0][2] == board[1][2] && board[0][2] == board[2][2])&& (board[0][2]=='O' ) ||
                (board[0][0] == board[1][1] && board[0][0] == board[2][2])&& (board[0][0]=='O' ) || (board[0][2] == board[1][1] && board[0][2] == board[2][0])&& (board[0][2]=='O' )){


            System.out.println("Second player won");


            return false;

        }

        else if (true){
            for(int i=0;i<3;i++){
                for (int j=0;j<3;j++){
                    if(board[i][j]==' ')
                        return true;
                }
            }
            System.out.println("with a draw");
            return false;
        }


        else{

            return true;

        }



    }
}